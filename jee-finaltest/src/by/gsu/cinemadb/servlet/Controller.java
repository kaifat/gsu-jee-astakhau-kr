package by.gsu.cinemadb.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.gsu.cinemadb.command.ActionCommand;
import by.gsu.cinemadb.command.factory.ActionFactory;
import by.gsu.cinemadb.resource.ConfigurationManager;
import by.gsu.cinemadb.resource.MessageManager;
@WebServlet("/controller")
public class Controller extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5798406057571196139L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
	private void processRequest(HttpServletRequest request,
			HttpServletResponse response)
					throws ServletException, IOException {
		
		response.setContentType("text/html; charset=UTF-8");
		request.setCharacterEncoding("UTF-8");
		String page = null;
		// define command from JSP
		ActionFactory client = new ActionFactory();
		ActionCommand command = client.defineCommand(request);
		/* Call the implemented execute () method and 
		 * pass parameters to the handler class of a particular command
		 */
		page = command.execute(request);
		// The method returns a response page
		// page = null; // try it!
		if (page != null) {
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
			// Call the response page
			dispatcher.forward(request, response);
		} else {
			// Setting up a page with an error message
			page = ConfigurationManager.getProperty("path.page.error");
			request.getSession().setAttribute("nullPage",
					MessageManager.getProperty("message.nullpage"));
			response.sendRedirect(request.getContextPath() + page);
		}
	}
}