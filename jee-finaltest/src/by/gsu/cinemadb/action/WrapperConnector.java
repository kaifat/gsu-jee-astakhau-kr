package by.gsu.cinemadb.action;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import by.gsu.cinemadb.resource.DatabaseConfigurationManager;


public class WrapperConnector {
	private Connection connection;

	public WrapperConnector() {
		try {
			String url = DatabaseConfigurationManager.getProperty("database.url");
			String user = DatabaseConfigurationManager.getProperty("database.user");
			String pass = DatabaseConfigurationManager.getProperty("database.password");			
			Properties prop = new Properties();
			prop.put("user", user);
			prop.put("password", pass);
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			connection = DriverManager.getConnection(url, prop);
		} catch (SQLException e) {
			System.err.println("not obtained connection " + e);
		}
	}

	public Statement getStatement() throws SQLException {

		if (connection != null) {
			Statement statement = connection.createStatement();
			if (statement != null) {
				return statement;
			}
		}
		throw new SQLException("connection or statement is null");
	}


	public PreparedStatement getPStatement(String sql) throws SQLException {
		if (connection != null) {
			PreparedStatement ps = connection.prepareStatement(sql);
			if (ps != null) {
				return ps;
			}
		}
		throw new SQLException("connection or statement is null");
	}

	public void closeStatement(Statement st) {
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				System.err.println("statement is null" + e);
			}
		} 
	}
	
	public void closePStatement(PreparedStatement ps) {
		if (ps != null) {
			try {
				ps.close();
			} catch (SQLException e) {
				System.err.println("statement is null" + e);
			}
		} 
	}

	public void closeConnection() {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				System.err.println(" wrong connection" + e);
			}
		} 
	}




}
