package by.gsu.cinemadb.command;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.gsu.cinemadb.dao.PeopleDAO;
import by.gsu.cinemadb.model.People;
import by.gsu.cinemadb.resource.ConfigurationManager;

public class ReadActorsWithMoreThenNFilmsCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		List<People> list = new ArrayList<>();
		PeopleDAO peopleDAO = new PeopleDAO();
		try {
			int count = Integer.parseInt(request.getParameter("count"));
			list = peopleDAO.getAllWithNFilms(count);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		request.setAttribute("peoples", list);
		
		page = ConfigurationManager.getProperty("path.page.allpeoples");
		return page;
	}

}
