package by.gsu.cinemadb.command;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.gsu.cinemadb.dao.PeopleDAO;
import by.gsu.cinemadb.model.People;
import by.gsu.cinemadb.resource.ConfigurationManager;

public class ReadSomePeoplesCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		List<People> list = new ArrayList<>();
		int type = 0;
		try {
			type = Integer.parseInt(request.getParameter("dropbox"));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		PeopleDAO peopleDAO = new PeopleDAO();
		switch (type) {
		case 1:
			list = peopleDAO.readAllActors();
			request.setAttribute("prof", "actors");
			break;
		case 2:
			list = peopleDAO.readAllProducers();
			request.setAttribute("prof", "producers");
			break;	
		case 3:
			list = peopleDAO.readPeopleWithBothProf();
			request.setAttribute("prof", "both");
			break;	
		default:
			break;
		}
		
	
	
		peopleDAO.close();
		request.setAttribute("peoples", list);

		page = ConfigurationManager.getProperty("path.page.allpeoples");
		return page;
	}

}
