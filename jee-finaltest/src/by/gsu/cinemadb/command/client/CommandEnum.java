package by.gsu.cinemadb.command.client;

import by.gsu.cinemadb.command.*;

public enum CommandEnum {
	READALLFILMS {
		{
			this.command = new ReadAllFilmsCommand();
		}
	},
	READACTORSWITHMORETHENNFILMS {
		{
			this.command = new ReadActorsWithMoreThenNFilmsCommand();
		}
	},
	READPEOPLESBYFILMID {
		{
			this.command = new ReadPeoplesByFilmIdCommand();
		}
		
	},
	READNEWFILMS {
		{
			this.command = new ReadNewFilmsCommand();
		}
	},
	READALLPEOPLES {
		{
			this.command = new ReadAllPeoplesCommand();
		}
	},
	READSOMEPEOPLES {
		{
			this.command = new ReadSomePeoplesCommand();
		}
	},
	CREATEACTOR {
		{
			this.command = new CreatePeopleCommand();
		}
	},
	CREATEFILM {
		{
			this.command = new CreateFilmCommand();
		}
	},
	DELETEFILMS {
		{
			this.command = new DeleteFilmsOverNYearsCommand();
		}
	},
	ADDACTORTOFILM {
		{
			this.command = new AddActorToFilmCommand();
		}
	};

			
	ActionCommand command;
	public ActionCommand getCurrentCommand() {
		return command;
	}
}
