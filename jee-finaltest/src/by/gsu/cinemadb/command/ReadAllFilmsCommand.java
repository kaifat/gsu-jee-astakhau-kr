package by.gsu.cinemadb.command;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.gsu.cinemadb.dao.FilmDAO;
import by.gsu.cinemadb.model.Film;
import by.gsu.cinemadb.resource.ConfigurationManager;

public class ReadAllFilmsCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		String page = null; 
		List<Film> films = new ArrayList<>();
		FilmDAO filmDAO = new FilmDAO();
		films = filmDAO.getAll();
		filmDAO.close();
		request.setAttribute("films", films);

		page = ConfigurationManager.getProperty("path.page.films");
		return page;
	}

}
