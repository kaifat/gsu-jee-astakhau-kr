package by.gsu.cinemadb.command;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.gsu.cinemadb.dao.PeopleDAO;
import by.gsu.cinemadb.model.People;
import by.gsu.cinemadb.resource.ConfigurationManager;

public class AddActorToFilmCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		List<People> list = new ArrayList<>();
		List<People> fullList = new ArrayList<>();
		PeopleDAO peopleDAO = new PeopleDAO();
		fullList = peopleDAO.readAll();
		
		try {
			int id = Integer.parseInt(request.getParameter("id"));
			String secondName = request.getParameter("dropbox");
			if (id!=0) {
				peopleDAO.addActorToFilm(secondName, id);
				list = peopleDAO.getByFilmId(id);
			}
			
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		peopleDAO.close();
		request.setAttribute("peoples", list);
		request.setAttribute("allpeoples", fullList);
		
		
		String page = ConfigurationManager.getProperty("path.page.peoples");
		return page;
	}

}
