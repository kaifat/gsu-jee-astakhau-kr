package by.gsu.cinemadb.command;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.gsu.cinemadb.dao.FilmDAO;
import by.gsu.cinemadb.model.Film;
import by.gsu.cinemadb.resource.ConfigurationManager;

public class DeleteFilmsOverNYearsCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		List<Film> films = new ArrayList<>();
		FilmDAO filmDAO = new FilmDAO();
		try {
			int n = Integer.parseInt(request.getParameter("years"));
			filmDAO.deleteFilms(n);	
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		films = filmDAO.getAll();
		filmDAO.close();
		request.setAttribute("films", films);

		page = ConfigurationManager.getProperty("path.page.films");
		return page;
	}

}
