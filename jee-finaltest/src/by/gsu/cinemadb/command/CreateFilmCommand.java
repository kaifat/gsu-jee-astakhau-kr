package by.gsu.cinemadb.command;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.gsu.cinemadb.dao.FilmDAO;
import by.gsu.cinemadb.model.Film;
import by.gsu.cinemadb.resource.ConfigurationManager;

public class CreateFilmCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		
		String title = request.getParameter("title");
		String country = request.getParameter("country");
		String date = request.getParameter("date");
		
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        java.sql.Date sql = null;
		try {
			Date parsed = format.parse(date);
			sql = new java.sql.Date(parsed.getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Film film = new Film(1, title, country, sql );
		List<Film> films = new ArrayList<>();
		FilmDAO filmDAO = new FilmDAO();
		filmDAO.insertFilm(film);
		films = filmDAO.getAll();
		filmDAO.close();
		request.setAttribute("films", films);
		
		page = ConfigurationManager.getProperty("path.page.films");
		return page;
	}

}
