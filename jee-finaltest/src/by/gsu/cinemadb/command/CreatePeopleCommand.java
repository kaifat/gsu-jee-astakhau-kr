package by.gsu.cinemadb.command;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.gsu.cinemadb.dao.PeopleDAO;
import by.gsu.cinemadb.model.People;
import by.gsu.cinemadb.resource.ConfigurationManager;

public class CreatePeopleCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		
		String firstName = request.getParameter("firstName");
		String secondName = request.getParameter("secondName");
		String date = request.getParameter("birthday");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        java.sql.Date sql = null;
		try {
			Date parsed = format.parse(date);
			sql = new java.sql.Date(parsed.getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		People people = new People(1, firstName, secondName, sql);
		List<People> peoples = new ArrayList<>();
		PeopleDAO peopleDAO = new PeopleDAO();
		peopleDAO.insertPeople(people);
		peoples = peopleDAO.readAll();
		peopleDAO.close();	

		request.setAttribute("peoples", peoples);
		page = ConfigurationManager.getProperty("path.page.allpeoples");
		return page;
	}

}
