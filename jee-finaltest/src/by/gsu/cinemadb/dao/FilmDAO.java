package by.gsu.cinemadb.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import by.gsu.cinemadb.model.Film;

public class FilmDAO extends AbstractDAO {

	public static final String SQL_SELECT_ALL_FROM_FILMS = "SELECT * from films;";
	public static final String SQL_INSERT_FILM = "INSERT films(title, country, release_date) VALUES (?,?,?);";
	public static final String SQL_SELECT_NEW_FILMS = "SELECT * from films WHERE films.release_date > (CURDATE() - INTERVAL 2 YEAR);";
	public static final String SQL_DELETE_FILMS = "DELETE FROM films WHERE films.release_date < (CURDATE() - INTERVAL ? YEAR);";

	public FilmDAO() {
		super();
	}
	
	// read all films
	public List<Film> getAll() {
		List<Film> list = new ArrayList<>();
		Statement st = null;
		try {
			st = connector.getStatement();
			ResultSet rs = st.executeQuery(SQL_SELECT_ALL_FROM_FILMS);
			while (rs.next()){
				int id = rs.getInt(1);
				String title  = rs.getString(2);
				String country = rs.getString(3);
				Date date = rs.getDate(4);
				list.add(new Film(id, title, country, date));
			}
		} catch (SQLException e) {
			System.err.println("SQL exception (request or table failed): " + e);
		} finally {
			this.closeStatement(st);
		}
		return list;
	}
	
	// read films with release date > current date + 2 year
	public List<Film> getNewFilms() {
		List<Film> list = new ArrayList<>();
		Statement st = null;
		try {
			st = connector.getStatement();
			ResultSet rs = st.executeQuery(SQL_SELECT_NEW_FILMS);
			while (rs.next()){
				int id = rs.getInt(1);
				String title  = rs.getString(2);
				String country = rs.getString(3);
				Date date = rs.getDate(4);
				list.add(new Film(id, title, country, date));
			}
		} catch (SQLException e) {
			System.err.println("SQL exception (request or table failed): " + e);
		} finally {
			this.closeStatement(st);
		}
		return list;
	}	

	// insert a movie into the database 
	public boolean insertFilm(Film film) {
		PreparedStatement ps = null;
		boolean res = false;
		try {
			ps = connector.getPStatement(SQL_INSERT_FILM);
			ps.setString(1, film.getTitle());
			ps.setString(2, film.getCountry());
			ps.setDate(3, film.getReleaseDate());
			res = ps.execute();
		} catch (SQLException e) {
			System.err.println("SQL exception (request or table failed): " + e);
		} finally {
			this.closePStatement(ps);
		}
		return res;
	}

	
	// delete films with release date older then current year - interval
	public boolean deleteFilms(int interval) {
		PreparedStatement ps = null;
		boolean res = false;
		try {
			ps = connector.getPStatement(SQL_DELETE_FILMS);
			ps.setInt(1, interval);
			res = ps.execute();
		} catch (SQLException e) {
			System.err.println("SQL exception (request or table failed): " + e);
		} finally {
			this.closePStatement(ps);
		}
		return res;
	}




}
