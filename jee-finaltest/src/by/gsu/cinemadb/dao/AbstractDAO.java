package by.gsu.cinemadb.dao;

import java.sql.PreparedStatement;
import java.sql.Statement;

import by.gsu.cinemadb.action.WrapperConnector;

public abstract class AbstractDAO {
	protected WrapperConnector connector;

			
	public AbstractDAO() {
		this.connector = new WrapperConnector();
	}

	public void close() {
		connector.closeConnection();
	}
	
	protected void closeStatement(Statement statement) {
		connector.closeStatement(statement);
	}
	
	protected void closePStatement(PreparedStatement pStatement) {
		connector.closePStatement(pStatement);
	}
}
