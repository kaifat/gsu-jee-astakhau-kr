package by.gsu.cinemadb.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import by.gsu.cinemadb.model.People;

public class PeopleDAO extends AbstractDAO {
	public static final String SQL_SELECT_ALL_FROM_PEOPLES = "SELECT * from peoples;";
	public static final String SQL_SELECT_ACTORS_FROM_PEOPLES =
			"SELECT DISTINCT peoples.id, peoples.firstname, peoples.secondname, peoples.birthday from peoples" +
					" JOIN films_peoples_prof ON peoples.id = films_peoples_prof.id_people" + 
					" JOIN films ON films_peoples_prof.id_film = films.id" +
					" WHERE films_peoples_prof.id_profession = 1;";

	public static final String SQL_SELECT_PEOPLES_WITH_BOTH_PROFESSION =
			"select peoples.id, peoples.firstname, peoples.secondname, peoples.birthday from peoples "
			+ "JOIN films_peoples_prof ON peoples.id = films_peoples_prof.id_people "
			+ "JOIN films ON films_peoples_prof.id_film = films.id  where id_profession in (1,2) "
			+ "group by id_people  having count(distinct id_profession ) = 2;";
	
	public static final String SQL_SELECT_PRODUCERS_FROM_PEOPLES = 
			"SELECT peoples.id, peoples.firstname, peoples.secondname, peoples.birthday from peoples" +
					" JOIN films_peoples_prof ON peoples.id = films_peoples_prof.id_people" + 
					" JOIN films ON films_peoples_prof.id_film = films.id" +
					" WHERE films_peoples_prof.id_profession = 2;";
	public static final String SQL_INSERT_PEOPLE = "INSERT peoples(firstname, secondname, birthday) VALUES (?,?,?);";

	public static final String SQL_SELECT_PEOPLES_BY_FILM = 
			"SELECT peoples.id, peoples.firstname, peoples.secondname, peoples.birthday from peoples" +  
					" JOIN films_peoples_prof ON peoples.id = films_peoples_prof.id_people" + 
					" JOIN films ON films_peoples_prof.id_film = films.id" +
					" WHERE films.id = ? AND films_peoples_prof.id_profession = 1;";


	public static final String SQL_SELECT_PEOPLE_BY_SECONDNAME = "SELECT id from peoples WHERE secondname = ?;";
	public static final String SQL_INSERT_PEOPLE_TO_FILM = "INSERT films_peoples_prof VALUES (?,?,1);";
	public static final String SQL_SELECT_PEOPLE_WITH_N_FILMS = 
			"SELECT peoples.id, peoples.firstname, peoples.secondname, peoples.birthday from peoples" +
					" JOIN films_peoples_prof ON peoples.id = films_peoples_prof.id_people" + 
					" JOIN films ON films_peoples_prof.id_film = films.id"	+
					" GROUP BY peoples.id HAVING COUNT(id_film)> ? ;";

	public PeopleDAO() {
		super();
	}
	
	// read all peoples
	public List<People> readAll() {
		List<People> list = new ArrayList<>();
		Statement st = null;
		try {
			st = connector.getStatement();
			ResultSet rs = st.executeQuery(SQL_SELECT_ALL_FROM_PEOPLES);
			while (rs.next()){
				int id = rs.getInt(1);
				String firstName  = rs.getString(2);
				String secondName = rs.getString(3);
				Date birthday = rs.getDate(4);
				list.add(new People(id, firstName, secondName, birthday));
			}
		} catch (SQLException e) {
			System.err.println("SQL exception (request or table failed): " + e);
		} finally {
			this.closeStatement(st);
		}

		return list;
	}
	
	
	// read peoples with both professions
	public List<People> readPeopleWithBothProf() {
		List<People> list = new ArrayList<>();
		Statement st = null;
		try {
			st = connector.getStatement();
			ResultSet rs = st.executeQuery(SQL_SELECT_PEOPLES_WITH_BOTH_PROFESSION);
			while (rs.next()){
				int id = rs.getInt(1);
				String firstName  = rs.getString(2);
				String secondName = rs.getString(3);
				Date birthday = rs.getDate(4);
				list.add(new People(id, firstName, secondName, birthday));
			}
		} catch (SQLException e) {
			System.err.println("SQL exception (request or table failed): " + e);
		} finally {
			this.closeStatement(st);
		}

		return list;
	}
	
	// read peoples with total films count > filmsCount
	public List<People> getAllWithNFilms(int filmsCount) {
		List<People> list = new ArrayList<>();
		PreparedStatement ps = null;
		try {
			ps = connector.getPStatement(SQL_SELECT_PEOPLE_WITH_N_FILMS);
			ps.setInt(1, filmsCount);
			ps.executeQuery();
			ResultSet rs = ps.executeQuery();
			while (rs.next()){
				int id = rs.getInt(1);
				String firstName  = rs.getString(2);
				String secondName = rs.getString(3);
				Date birthday = rs.getDate(4);
				list.add(new People(id, firstName, secondName, birthday));
			}
		} catch (SQLException e) {
			System.err.println("SQL exception (request or table failed): " + e);
		} finally {
			this.closePStatement(ps);
		}
		return list;
	}
	
	// read peoples from film (films.id = code )
	public List<People> getByFilmId(int code) {
		List<People> list = new ArrayList<>();
		PreparedStatement ps = null;
		try {
			ps = connector.getPStatement(SQL_SELECT_PEOPLES_BY_FILM);
			ps.setInt(1, code);
			ps.executeQuery();
			ResultSet rs = ps.executeQuery();
			while (rs.next()){
				int id = rs.getInt(1);
				String firstName  = rs.getString(2);
				String secondName = rs.getString(3);
				Date birthday = rs.getDate(4);
				list.add(new People(id, firstName, secondName, birthday));
			}
		} catch (SQLException e) {
			System.err.println("SQL exception (request or table failed): " + e);
		} finally {
			this.closePStatement(ps);
		}
		return list;
	}

	// read peoples who are actors
	public List<People> readAllActors() {
		List<People> list = new ArrayList<>();
		Statement st = null;
		try {
			st = connector.getStatement();
		//	ResultSet rs = st.executeQuery(SQL_SELECT_ALL_FROM_PEOPLES);
			ResultSet rs = st.executeQuery(SQL_SELECT_ACTORS_FROM_PEOPLES);
			while (rs.next()){
				int id = rs.getInt(1);
				String firstName  = rs.getString(2);
				String secondName = rs.getString(3);
				Date birthday = rs.getDate(4);
				list.add(new People(id, firstName, secondName, birthday));
			}
		} catch (SQLException e) {
			System.err.println("SQL exception (request or table failed): " + e);
		} finally {
			this.closeStatement(st);
		}
		return list;
	}

	// read peoples who are producers
	public List<People> readAllProducers() {
		List<People> list = new ArrayList<>();
		Statement st = null;
		try {
			st = connector.getStatement();
			ResultSet rs = st.executeQuery(SQL_SELECT_PRODUCERS_FROM_PEOPLES);
			while (rs.next()){
				int id = rs.getInt(1);
				String firstName  = rs.getString(2);
				String secondName = rs.getString(3);
				Date birthday = rs.getDate(4);
				list.add(new People(id, firstName, secondName, birthday));
			}
		} catch (SQLException e) {
			System.err.println("SQL exception (request or table failed): " + e);
		} finally {
			this.closeStatement(st);
		}
		return list;
	}

	// create people
	public boolean insertPeople(People people) {
		PreparedStatement ps = null;
		boolean res = false;
		try {
			ps = connector.getPStatement(SQL_INSERT_PEOPLE);
			ps.setString(1, people.getFirstName());
			ps.setString(2, people.getSecondName());
			ps.setDate(3, people.getBirthday());
			res = ps.execute();
		} catch (SQLException e) {
			System.err.println("SQL exception (request or table failed): " + e);
		} finally {
			this.closePStatement(ps);
		}

		return res;
	}
	
	// add actor with secondname= secondName to film with film.id = fid
	public boolean addActorToFilm(String secondName, int fid) {
		PreparedStatement ps0 = null;
		PreparedStatement ps = null;
		boolean res = false;
		try {
			ps0 = connector.getPStatement(SQL_SELECT_PEOPLE_BY_SECONDNAME);
			ps0.setString(1, secondName);
			ResultSet resultSet = ps0.executeQuery();
			resultSet.first();
			int aid = resultSet.getInt(1);

			ps = connector.getPStatement(SQL_INSERT_PEOPLE_TO_FILM);
			ps.setInt(1, fid);
			ps.setInt(2, aid);

			res = ps.execute();
		} catch (SQLException e) {
			System.err.println("SQL exception (request or table failed): " + e);
		} finally {
			this.closePStatement(ps);
			this.closePStatement(ps0);
		}


		return res;

	}
}
