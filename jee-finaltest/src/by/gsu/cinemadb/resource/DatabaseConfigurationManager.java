package by.gsu.cinemadb.resource;

import java.util.ResourceBundle;

public class DatabaseConfigurationManager {
	private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.database");
	// The class extracts information from the database.properties file
	private DatabaseConfigurationManager() { }
	public static String getProperty(String key) {
		return resourceBundle.getString(key);
	}
}
