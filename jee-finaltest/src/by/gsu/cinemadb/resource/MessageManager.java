package by.gsu.cinemadb.resource;

import java.util.ResourceBundle;
public class MessageManager {
	private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.messages");
	// The class extracts information from the messages.properties file
	private MessageManager() { }
	public static String getProperty(String key) {
		return resourceBundle.getString(key);
	}
}
