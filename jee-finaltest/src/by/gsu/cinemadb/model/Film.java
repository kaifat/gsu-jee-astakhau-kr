package by.gsu.cinemadb.model;

import java.sql.Date;

public class Film extends Entity {
	private String title;
	private String country;
	private Date releaseDate;

	public Film() {

	}

	public Film(int id, String title, String country, Date releaseDate) {
		super(id);
		this.title = title;
		this.country = country;
		this.releaseDate = releaseDate;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	@Override
	public String toString() {
		return "Film [title=" + title + ", country=" + country + ", releaseDate=" + releaseDate + "]";
	}
	
	
}
