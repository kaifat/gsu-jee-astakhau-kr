package by.gsu.cinemadb.model;

import java.sql.Date;

public class People extends Entity {
	private String firstName;
	private String secondName;
	private Date birthday;
	
	public People() {
	}


	public People(int id, String firstName, String secondName, Date birthday) {
		super(id);
		this.firstName = firstName;
		this.secondName = secondName;
		this.birthday = birthday;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getSecondName() {
		return secondName;
	}


	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}


	public Date getBirthday() {
		return birthday;
	}


	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}



	@Override
	public String toString() {
		return "People [firstName=" + firstName + ", secondName=" + secondName + ", birthday=" + birthday + "]";
	}



	
	
	
	
	
}
