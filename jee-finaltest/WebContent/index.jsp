<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>cinemadb</title>
<link rel="stylesheet" href="css/style.css">
</head>
<body>

<%@ include file="blocks/menu.html"%>

<div class="main">
	<h1>JEE, Контрольная работа , вариант #1</h1>	
	
	<br/>
	Видеотека. 
	<br/>
	В БД хранится информация о домашней видеотеке – фильмы, актеры, режиссеры. 
	<br/>
	Для фильмов необходимо хранить: название, имена актеров, дату выхода, страну.
	<br/>
	Для актеров и режиссеров необходимо хранить: ФИО, дату рождения. 
	<br/>
	· Найти все фильмы, вышедшие на экран в текущем и прошлом году.
	<br/>
	· Вывести информацию об актерах, снимавшихся в заданном фильме.
	<br/>
	· Вывести информацию об актерах, снимавшихся как минимум в N фильмах.
	<br/>
	· Вывести информацию об актерах, которые были режиссерами хотя бы одного из фильмов.
	<br/>
	· Удалить все фильмы, дата выхода которых была более заданного числа лет назад. 
	
</div>

</body>
</html>