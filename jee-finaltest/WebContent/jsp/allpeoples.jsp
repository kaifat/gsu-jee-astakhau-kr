<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>cinemadb : all peoples</title>
<link rel="stylesheet" href="css/style.css">
</head>
<body>

	<%@ include file="../blocks/menu.html"%>

	<div class="main">
		<h1>All Peoples :  ${ prof }</h1>
		<form method="POST" action="controller">
			<input type="hidden" name="command" value="readSomePeoples"/>
			<p align="center">
			<br/>
			<select name="dropbox">
				<option value="1">actors</option>
				<option value="2">producers</option>
				<option value="3">actors who also producers</option>
			</select>
			
			<input type="submit" value="show"/>
			</p>
		</form>
		
		
		
		
		<table class="container">

			<thead>
				<tr>
					<th><h1>Id</h1></th>
					<th><h1>First name</h1></th>
					<th><h1>Second name</h1></th>
					<th><h1>Birthday</h1></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="elem" items="${peoples}" varStatus="status">
					<tr>
						<td><c:out value="${ elem.getId() }"></c:out></td>
						<td><c:out value="${ elem.getFirstName() }"></c:out></td>
						<td><c:out value="${ elem.getSecondName() }"></c:out></td>
						<td><c:out value="${ elem.getBirthday() }"></c:out></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		
		<form method="POST" action="controller">
			<input type="hidden" name="command" value="readActorsWithMoreThenNFilms" />
			<p align="center">
			show only actors with n films :
			<br/>
			<input type="text" name="count" value=""/>
			<input type="submit" value="show" />
			</p>	
		</form>
		
		
		<form method="POST" action="controller">
			<input type="hidden" name="command" value="createActor"/>
			<p align="center">
			<br/>
			First Name:
			<br/>
			<input type="text" name="firstName" value=""/>
			<br/>
			Second Name:
			<br/>
			<input type="text" name="secondName" value=""/>
			<br/>
			Birthday:
			<br/>
			<input type="text" name="birthday" value=""/>
			<br/>
			
			<input type="submit" value="create new people"/>
			</p>
		</form>
		

	</div>

</body>
</html>