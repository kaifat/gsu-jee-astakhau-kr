<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>cinemadb : films</title>
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<%@ include file="../blocks/menu.html"%>

	<div class="main">
		<h1>Films</h1>
		<form method="POST" action="controller">
			<input type="hidden" name="command" value="readNewFilms" />
			<p align="center">
				<input type="submit" value="show only new films" />
			</p>
		</form>
		<table class="container">
			<thead>
				<tr>
					<th><h1>Id</h1></th>
					<th><h1>Title</h1></th>
					<th><h1>Country</h1></th>
					<th><h1>Release date</h1></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="elem" items="${films}" varStatus="status">
					<tr>
						<td><a
							href="controller?command=readPeoplesByFilmId&id=${ elem.getId() }"><c:out
									value="${ elem.getId() }"></c:out></a></td>
						<td><c:out value="${ elem.getTitle() }"></c:out></td>
						<td><c:out value="${ elem.getCountry() }"></c:out></td>
						<td><c:out value="${ elem.getReleaseDate() }"></c:out></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>


		<form method="POST" action="controller">
			<input type="hidden" name="command" value="createFilm" />
			<p align="center">
				Title: <br /> <input type="text" name="title" value="" />
				<br /> Country: <br /> <input type="text" name="country" value="" />
				<br /> Date: <br /> <input type="text" name="date" value="" /> <br />
				<input type="submit" value="add new film" />
			</p>
		</form>

		<form method="POST" action="controller">
			<input type="hidden" name="command" value="deleteFilms" />
			<p align="center">
				Delete films with release date over N years : <br /> <input
					type="text" name="years" value="" /> <input type="submit"
					value="delete films" />
			</p>
		</form>
	</div>
</body>
</html>