<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>cinemadb : peoples</title>
<link rel="stylesheet" href="css/style.css">
</head>
<body>

	<%@ include file="../blocks/menu.html"%>

	<div class="main">
		<h1>Peoples</h1>
		<table class="container">

			<thead>
				<tr>
					<th><h1>Id</h1></th>
					<th><h1>First name</h1></th>
					<th><h1>Second name</h1></th>
					<th><h1>Birthday</h1></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="elem" items="${peoples}" varStatus="status">
					<tr>
						<td><c:out value="${ elem.getId() }"></c:out></td>
						<td><c:out value="${ elem.getFirstName() }"></c:out></td>
						<td><c:out value="${ elem.getSecondName() }"></c:out></td>
						<td><c:out value="${ elem.getBirthday() }"></c:out></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		
		
		<form name="AddForm" method="POST" action="controller">
			<input type="hidden" name="command" value="addActorToFilm"/>
			<input type="hidden" name="id" value="${ id }"/>
			<p align="center">
			<br/>
			<select name="dropbox">
			<c:forEach var="elem" items="${allpeoples}" varStatus="status">
		 		 <option>${ elem.getSecondName() }</option>
		 	
		 	 </c:forEach>
			</select>
			
			<input type="submit" value="add actor to film"/>
			</p>
		</form>

	</div>

</body>
</html>